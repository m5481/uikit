module.exports = {
  configureWebpack: {
    devServer: {
      port: 11000,
    },
    resolve: {
      extensions: ['*', '.js', '.vue', '.json'],
    },
  },
};
