import Vue from 'vue';
import VueRouter from 'vue-router';
import components from '@/router/components';
import Home from '../views/Home';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Home page',
    },
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About'),
    meta: {
      title: 'About page',
    },
  },
  components,
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to) => {
  if (to && to.meta && to.meta.title) {
    document.title = to.meta.title;
  } else {
    document.title = 'UiKit';
  }
});

export default router;
