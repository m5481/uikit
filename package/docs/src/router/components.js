export default {
  path: '/components',
  name: 'ComponentsContainer',
  component: () => import('../views/ComponentsContainer'),
  meta: {
    title: 'Components Container',
    menu: {
      name: 'Компоненты',
    },
  },
  children: [{
    path: '/button',
    name: 'button',
    component: () => import('../views/Components/Button'),
    meta: {
      title: 'ui-button',
      menu: {
        name: 'Кнопка',
      },
    },
  }, {
    path: '/link',
    name: 'link',
    component: () => import('../views/Components/Link'),
    meta: {
      title: 'ui-link',
      menu: {
        name: 'Ссылка',
      },
    },
  }],
};
