/* eslint-disable import/prefer-default-export */
import { Vue } from '../../vue';

const props = {};

export const UiLink = Vue.extend({
  name: 'UiLink',
  functional: true,
  props,
  render(h, { children }) {
    return h('a', children);
  },
});
