import componentsPlugins from './components';

const registerComponents = (Vue, components) => {
  components.forEach(({ name, component }) => {
    Vue.component(name, component);
    console.log(name, component);
  });
};

export default (Vue) => {
  console.log(componentsPlugins);
  registerComponents(Vue, Object.keys(componentsPlugins).map((name) => ({
    name,
    component: componentsPlugins[name],
  })));
};
